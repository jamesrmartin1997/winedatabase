package org.winebasement.winedatabase;

/**
 * Wine Class
 *
 * Represents an individual wine in the database.
 * Contains methods for getting all information, and methods for setting price and quantity.
 * @author James Martin
 * @version 1.0
 * @since 2018-05-16
 */

public class Wine {

    //Wine attributes stored in database
    private String Title;
    private String Variety;
    private String Winery;
    private String Country;
    private String Province;
    private String Region;
    private String Designation;
    private String Description;
    private int Points;
    private int Price;
    private int Quantity;
    private int WineID;

    /**
     * Constructor, creates a wine.
     *
     * @param title       String, Name of the wine.
     * @param variety     String, Variety of wine (e.g. Red, White, etc)
     * @param winery      String, Name of company producing the wine
     * @param country     String, Name of origin country
     * @param province    String, Name of origin province
     * @param region      String, name of origin region
     * @param designation String, Designation by winery (e.g. vintage, special variant, etc)
     * @param description String, tasting notes
     * @param points      int, review score for the wine
     * @param price       int, price for a single bottle
     * @param quantity    int, Amount currently in stock
     * @param id         int, WineID number from CSV
     */
    public Wine(String title, String variety, String winery, String country,
                String province, String region, String designation, String description,
                int points, int price, int quantity, int id) {
        Title = title;
        Variety = variety;
        Winery = winery;
        Country = country;
        Province = province;
        Region = region;
        Designation = designation;
        Description = description;
        Points = points;
        Price = price;
        Quantity = quantity;
        WineID = id;
    }

    /**
     * Gets name of wine.
     *
     * @return String, name of wine
     */
    public String getTitle() {
        return Title;
    }

    /**
     * Gets variety of wine (e.g. Red, White, Pinot Noir).
     *
     * @return String, wine variety
     */
    public String getVariety() {
        return Variety;
    }

    /**
     * Gets winery, the company responsible for producing the wine.
     *
     * @return String, name of winery
     */
    public String getWinery() {
        return Winery;
    }

    /**
     * Gets country of origin, where the wine was created..
     *
     * @return String, name of country
     */
    public String getCountry() {
        return Country;
    }

    /**
     * Gets province where the wine was produced.
     *
     * @return String, name of province
     */
    public String getProvince() {
        return Province;
    }

    /**
     * Gets name of region in province, where whine was produced.
     *
     * @return String, name of region
     */
    public String getRegion() {
        return Region;
    }

    /**
     * Gets designation of wine.
     *
     * @return String, designation of wine
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * Gets tasting notes for a given wine.
     *
     * @return String, tasting notes
     */
    public String getDescription() {
        return Description;
    }

    /**
     * Gets wine review scores.
     *
     * @return int, review score
     */
    public int getPoints() {
        return Points;
    }

    /**
     * Gets price of wine.
     *
     * @return int, price of wine
     */
    public int getPrice() {
        return Price;
    }

    /**
     * Gets quantity of wine currently in stock.
     *
     * @return int, quantity of wine currently in stock
     */
    public int getQuantity() {
        return Quantity;
    }

    public int getWineID() {
        return WineID;
    }
}

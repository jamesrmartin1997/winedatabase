package org.winebasement.winedatabase;


import org.apache.commons.csv.*;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * WineList Class
 *
 * Used for representing a list of wines in the application.
 * Includes functions for adding, updating, and deleting items, as well as saving the list.
 *
 * @author James Martin
 * @version 0.5
 * @since 2018-05-16
 */
public class WineList {

    //Class Variables
    private ArrayList<Wine> winelist;
    private String filepath;
    private int recordCount;

    /**
     * Constructor, populates wine list from CSV file
     * @param fpath Filepath where CSV file is stored
     */
    public WineList(String fpath) {
        //Initialises class variables
        winelist = new ArrayList<>();
        filepath = fpath;
        recordCount = 0;
        //Initialises Commons CSV
        try {
            Reader in = new FileReader(filepath);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
            //Loop loads individual records, stores as objects and adds to the arraylist
            for (CSVRecord record : records) {
                //Stores record fields in local variables
                int id = Integer.parseInt(record.get("wine_id"));
                String country = record.get("country");
                String description = record.get("description");
                String designation = record.get("designation");
                int points = Integer.parseInt(record.get("points"));
                int price = Integer.parseInt(record.get("price"));
                String province = record.get("province");
                String region = record.get("region");
                String title = record.get("title");
                String variety = record.get("variety");
                String winery = record.get("winery");
                int quantity = Integer.parseInt(record.get("quantity"));
                //Creates object and adds to the arraylist
                Wine wine = new Wine(title, variety, winery, country, province,
                        region, designation, description, points, price, quantity, id);
                winelist.add(wine);
                recordCount++;
            }
        } catch (IOException e) {
            System.out.print("IO Exception when loading CSV file.");
            e.printStackTrace();
        }
    }

    /**
     * Adds a wine to the list, if it is not already present.
     * @param wine Wine, New wine to be added to list
     * @return Boolean, True if successful, false if not
     */
    public boolean addRecord (Wine wine) {
        if (!winelist.contains(wine)) {
            winelist.add(wine);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Replaces a wine in the database
     * @param wine Wine, new object to replace original in list
     * @return Boolean, True if successful, false if not
     */
    public boolean appendRecord(Wine wine) {
        Wine oldwine = getWineByID(wine.getWineID());
        //Exits function if wine does not exist in list
        if (oldwine == null) {
            return false;
        }
        int index = winelist.indexOf(oldwine);
        winelist.set(index, wine);
        return true;
    }

    /**
     * Deletes a wine in the wine list.
     * @param wine Object to be deleted from the list
     * @return Boolean, True if successful, false if not
     */
    public boolean deleteRecord(Wine wine) {
        if (winelist.contains(wine)) {
            winelist.remove(wine);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Saves the contents of the wine list to a CSV file.
     * @return Boolean, true if successful, false if not
     */
    public boolean saveList() {
        try {
            FileWriter out= new FileWriter(filepath);
            CSVPrinter csvFilePrinter = new CSVPrinter(out, CSVFormat.EXCEL.withHeader("",""));
            for (Wine w: winelist) {
                csvFilePrinter.printRecord(w.getWineID(), w.getCountry(), w.getDescription(), w.getDesignation(),
                        w.getPoints(), w.getPrice(), w.getProvince(), w.getRegion(), w.getTitle(), w.getVariety(),
                        w.getWinery(), w.getQuantity());
            }
            out.flush();
            out.close();
            csvFilePrinter.close();
            return true;
        } catch (IOException e) {
            System.out.print("IO Exception occurred when saving Wine List");
            e.printStackTrace();
            return false;
        }
    }

    //Sort List

    /**
     * Implements binary search to get a desired wine from the list.
     * @param id int, ID of desired wine in list
     * @return Desired wine from list
     */
    public Wine searchByID(int id) {
        int left = winelist.get(winelist.size()).getWineID();
        int right = winelist.get(0).getWineID();
        int mid = winelist.size() / 2;
        int nItems = winelist.size();
        int k = 7;
        //
        while (nItems > k) {
            nItems = nItems / 2;
            //
            if(id < winelist.get(mid).getWineID()) {
                right = mid;
            } else {
                left = mid;
            }
        }
        //Linear search on reduced pool
        for (int i = left; i == right; i++) {
            Wine wine = winelist.get(i);
            if (wine.getWineID() == i) {
                return wine;
            }
        }
    }
    /**
     * Gets a wine from the list using a loop to search linearly.
     * @param id int, ID number of desired record
     * @return Wine, desired object in wine list. Null if not successful.
     */
    public Wine getWineByID(int id) {
        for (Wine wine: winelist) {
            if (wine.getWineID() == id) {
                return wine;
            }
        }
        return null;
    }

    /**
     * Gets current wine list. For use when displaying on-screen.
     * @return ArrayList containing all Wine objects
     */
    public ArrayList<Wine> getWineList() {
        return winelist;
    }
}

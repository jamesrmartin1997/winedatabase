package org.winebasement.winedatabase;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Class Main.
 *
 * Initialises application, opens window and homepage.
 * @version 1.0
 * @since 2018-05-18
 * @author James Martin
 */

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        Parent homepage = null;
        try {
            homepage = FXMLLoader.load(getClass().getResource("homepage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scHomepage = new Scene(homepage, 1280, 720);
        stage.setTitle("Wine Database");
        stage.setScene(scHomepage);
        stage.show();
    }
}

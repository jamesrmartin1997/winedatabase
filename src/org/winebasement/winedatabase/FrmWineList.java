package org.winebasement.winedatabase;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class FrmWineList {

    WineList winelist;

    //FXML Controls
    @FXML private TableView<Wine> tblWineList;
    @FXML private TableColumn<Wine, Integer> clmID;
    @FXML private TableColumn<Wine, String> clmTitle;
    @FXML private TableColumn<Wine, String> clmVariety;
    @FXML private TableColumn<Wine, String> clmWinery;
    @FXML private TableColumn<Wine, String> clmCountry;
    @FXML private TableColumn<Wine, String> clmProvince;
    @FXML private TableColumn<Wine, String> clmRegion;
    @FXML private TableColumn<Wine, String> clmDesignation;
    @FXML private TableColumn<Wine, Integer> clmScore;
    @FXML private TableColumn<Wine, Integer> clmPrice;
    @FXML private TableColumn<Wine, Integer> clmQuantity;
    @FXML private TableColumn<Wine, String> clmDescription;

    @FXML
    public void initialize() {
        //Loads winelist from CSV file
        File file = new File("resources/winedatatest.csv");
        String absolutePath = file.getAbsolutePath();
        winelist = new WineList(absolutePath);
        //Populates table with wines
        clmID.setCellValueFactory(new PropertyValueFactory<>("WineID"));
        clmTitle.setCellValueFactory(new PropertyValueFactory<>("Title"));
        clmVariety.setCellValueFactory(new PropertyValueFactory<>("Variety"));
        clmWinery.setCellValueFactory(new PropertyValueFactory<>("Winery"));
        clmCountry.setCellValueFactory(new PropertyValueFactory<>("Country"));
        clmProvince.setCellValueFactory(new PropertyValueFactory<>("Province"));
        clmRegion.setCellValueFactory(new PropertyValueFactory<>("Region"));
        clmDesignation.setCellValueFactory(new PropertyValueFactory<>("Designation"));
        clmScore.setCellValueFactory(new PropertyValueFactory<>("Points"));
        clmPrice.setCellValueFactory(new PropertyValueFactory<>("Price"));
        clmQuantity.setCellValueFactory(new PropertyValueFactory<>("Quantity"));
        clmDescription.setCellValueFactory(new PropertyValueFactory<>("Description"));
        tblWineList.getItems().addAll(winelist.getWineList());
    }

    @FXML
    public void handleButtonReturnMenu(ActionEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Parent homepage = null;
        try {
            homepage = FXMLLoader.load(getClass().getResource("homepage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scHomepage = new Scene(homepage, 1280, 720);
        stage.setTitle("Wine Database");
        stage.setScene(scHomepage);
    }
}

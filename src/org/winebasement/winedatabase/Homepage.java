package org.winebasement.winedatabase;

import javafx.fxml.*;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.event.ActionEvent;
import java.io.IOException;

public class Homepage {

    /*
     * Loads the wine list window when clicked.
     */
    @FXML
    public void handleButtonWineList(ActionEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Parent winepage = null;
        try {
            winepage = FXMLLoader.load(getClass().getResource("frmWineList.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scWinepage = new Scene(winepage, 1280, 720);
        stage.setTitle("View Wine List");
        stage.setScene(scWinepage);
    }

}

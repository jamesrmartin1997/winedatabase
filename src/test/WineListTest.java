package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.winebasement.winedatabase.Wine;
import org.winebasement.winedatabase.WineList;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class WineListTest {

    String result;
    WineList winelist;

    @BeforeEach
    public void setup() {
        File file = new File("resources/winedatatest.csv");
        String absolutePath = file.getAbsolutePath();
        winelist = new WineList(absolutePath);
    }

    @Test
    public void testGetRecord() {
        Wine wine = winelist.getWineByID(5);
        assertEquals(5, wine.getWineID());
    }

    //Sort list
    //Search list

    @Test
    public void testAddRecord() {
        Wine wine = new Wine("Leon Beyer 2012 Gewurztraminer (Alsace)", "GewÃ¼rztraminer", "Leon Beyer",
                "France","Alsace", "Alsace", "", "This is a dry wine, very spicy, with a tight, taut texture and strongly mineral character layered with citrus as well as pepper. It's a food wine with its almost crisp aftertaste.",
                87, 30, 0, 11);
        assertTrue(winelist.addRecord(wine));
    }

    @Test
    public void testAddDuplicateRecord() {
        Wine wine = new Wine("Sweet Cheeks 2012 Vintner's Reserve Wild Child Block Pinot Noir (Willamette Valley)","Pinot Noir",
                "Sweet Cheeks", "US", "Oregon", "Willamette Valley",
                "Vintner's Reserve Wild Child Block", "Much like the regular bottling from 2012, this comes across as rather rough and tannic, with rustic, earthy, herbal characteristics. Nonetheless, if you think of it as a pleasantly unfussy country wine, it's a good companion to a hearty winter stew.",
                87,65,0,4);
        assertFalse(winelist.addRecord(wine));
    }

    @Test
    public void testAppendRecord() {
        Wine wine = new Wine("St. Julian 2013 Reserve Late Harvest Riesling (Lake Michigan Shore)","Riesling",
                "St. Julian", "US", "Michigan", "Lake Michigan Shore",
                "Reserve Late Harvest", "Pineapple rind, lemon pith and orange blossom start off the aromas. The palate is a bit more opulent, with notes of honey-drizzled guava and mango giving way to a slightly astringent, semidry finish.",
                87,13,2,3);
        assertTrue(winelist.appendRecord(wine));
    }

    @Test
    public void testDeleteRecord() {
        Wine wine = winelist.getWineByID(10);
        assertTrue(winelist.deleteRecord(wine));
    }

    @Test
    public void testSaveList() {
        assertTrue(winelist.saveList());
    }
}